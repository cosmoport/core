package com.cosmoport.core.persistence.exception;

public final class JsonConvertException extends Exception {
    private static final long serialVersionUID = -5418408431565956052L;
}
