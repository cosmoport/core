# Cosmoport Core
> The application core v0.1.1

[![build status](https://gitlab.com/cosmoport/core/badges/master/build.svg)](https://gitlab.com/cosmoport/core/commits/master) | [Changelog](./CHANGELOG.md) | [API](./doc/API.md)

### Uses
- Java 8
- Jetty 9 (Server)
- Resteasy (JAX-RS)
- Jackson (JSON)
- Guice (DI)
- Guice-async (Scheduler)
- SQLite (Db)
- HikariCP (Db pool)
- Flyway (Db migrations)
- SLF4J, Logback (Logging)
- Junit (Testing)

### Default settings
- Host: **127.0.0.1** (localhost)
- Port: **8080**
- Socket timeout: **0** (Infinite)

### Run
In order to run the application you should install JRE 8+.
To run the application you should execute the following script:
```text
gradlew run
```